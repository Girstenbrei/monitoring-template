# Monitoring Template

Prerequisites:
- Docker
- docker-compose
- working app in `./app`

Then run: `docker-compose up -d`

Services listening:
- Prometheus: http://localhost:9090
- Grafana: http://localhost:3000

Prometheus is trying to scrape the app, see http://localhost:9090/targets.
